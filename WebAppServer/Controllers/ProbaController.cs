﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using WebAppServer.Models.Entities;
using WebAppServer.Models;


namespace WebAppServer.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ProbaController : ApiController
    {
        private DbManager dbManager = DbManager.GetInstance();

        [HttpGet]
        public List<People> GetPeoples()
        {
            return dbManager.TablePeople.GetAll();
        }

        [HttpPost]
        [ActionName("InsertPeople")]
        public void InsertPeople(People people)
        {
            dbManager.TablePeople.AddNewPeople(people);
        }

        [HttpPost]
        [ActionName("FindPeoplesById")]
        public List<People> FindPeoplesById([FromBody]int id)
        {
            return dbManager.TablePeople.FindPeopleById(id);
        }

        [HttpPost]
        [ActionName("UpdatePeople")]
        public void UpdatePeople(People people)
        {
            dbManager.TablePeople.UpdatePeopleById(people);
        }

        [HttpPost]
        [ActionName("DeletePeople")]
        public void DeletePeople([FromBody]int id)
        {
            dbManager.TablePeople.DeleteById(id);
        }
    }
}
