﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAppServer.Models.Tables;

namespace WebAppServer.Models
{
    public class DbManager
    {
        private static DbManager instance = null;

        public static DbManager GetInstance()
        {
            if (instance == null)
            {
                instance = new DbManager();
            }

            return instance;
        }

        public TablePeoples TablePeople { get; private set; }


        public DbManager()
        {
            string connectionString = "Server=localhost;User=root;Password=1234;Database=data_peoples";

            TablePeople = new TablePeoples(connectionString);

        }
    }
}