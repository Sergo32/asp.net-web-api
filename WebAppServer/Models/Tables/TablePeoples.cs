﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAppServer.Models.Entities;
using MySql.Data.MySqlClient;

namespace WebAppServer.Models.Tables
{
    public class TablePeoples
    {
        private string connectionString;

        public TablePeoples(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<People> FindPeopleById(int id)

        {
            List<People> peoples = null;
            try
            {
                using (MySqlConnection mySqlConnection = new MySqlConnection(connectionString))
                {
                    mySqlConnection.Open();

                    using (MySqlCommand mySqlCommand = new MySqlCommand())
                    {
                        mySqlCommand.Connection = mySqlConnection;
                        mySqlCommand.CommandText = $"Select * From `peoples` where `id`={id}";

                        MySqlDataReader reader = mySqlCommand.ExecuteReader();

                        peoples = new List<People>();

                        while (reader.Read())
                        {
                            People people = new People()
                            {
                                Id = reader.GetInt32("id"),
                                Name = reader.GetString("name"),
                                Age = reader.GetInt32("age")


                            };


                            peoples.Add(people);
                        }
                    }

                    mySqlConnection.Close();
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e);
                throw;
            }

            return peoples;
        }


        public List<People> GetAll()

        {
            List<People> peoples = null;
            try
            {
                using (MySqlConnection mySqlConnection = new MySqlConnection(connectionString))
                {
                    mySqlConnection.Open();

                    using (MySqlCommand mySqlCommand = new MySqlCommand())
                    {
                        mySqlCommand.Connection = mySqlConnection;
                        mySqlCommand.CommandText = $"Select * From `peoples` ";

                        MySqlDataReader reader = mySqlCommand.ExecuteReader();

                        peoples = new List<People>();

                        while (reader.Read())
                        {
                            People people = new People()
                            {
                                Id = reader.GetInt32("id"),
                                Name = reader.GetString("name"),
                                Age = reader.GetInt32("age")


                            };


                            peoples.Add(people);
                        }
                    }

                    mySqlConnection.Close();
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e);
                throw;
            }

            return peoples;
        }
        public bool AddNewPeople(People people)
        {
            try
            {

                using (MySqlConnection mySqlConnection = new MySqlConnection(connectionString))
                {
                    mySqlConnection.Open();

                    using (MySqlCommand mySqlCommand = new MySqlCommand())
                    {
                        mySqlCommand.Connection = mySqlConnection;
                        mySqlCommand.CommandText = $"INSERT INTO `peoples`(`name`,`age`) VALUES('{people.Name}',{people.Age})";

                        mySqlCommand.ExecuteNonQuery();
                    }
                    mySqlConnection.Close();
                }

                return true;
            }
            catch
            {
                return false;
            }
        }
       


        public bool UpdatePeopleById(People people)
        {
            try
            {
                using (MySqlConnection mySqlConnection = new MySqlConnection(connectionString))
                {
                    mySqlConnection.Open();

                    using (MySqlCommand mySqlCommand = new MySqlCommand())
                    {
                        mySqlCommand.Connection = mySqlConnection;
                        mySqlCommand.CommandText = $@"Update `peoples` SET `name`='{people.Name}',`age`={people.Age} WHERE id={people.Id}";

                        mySqlCommand.ExecuteNonQuery();
                    }
                    mySqlConnection.Close();
                }

                return true;
            }
            catch
            {
                return false;
            }

        }

        public bool DeleteById(int id)
        {
            try
            {
                using (MySqlConnection mySqlConnection = new MySqlConnection(connectionString))
                {
                    mySqlConnection.Open();

                    using (MySqlCommand mySqlCommand = new MySqlCommand())
                    {
                        mySqlCommand.Connection = mySqlConnection;
                        mySqlCommand.CommandText = $@"DELETE FROM `peoples` WHERE `id`={id}";

                        mySqlCommand.ExecuteNonQuery();
                    }
                    mySqlConnection.Close();
                }

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}