let nameField = document.getElementById("nameField");
let ageField = document.getElementById("ageField");
let idField = document.getElementById("idField");
let peopleDiv = document.getElementById("peopleDiv");
let idFieldFaind = document.getElementById("idFieldFaind");
let nameFieldUpdate = document.getElementById("nameFieldUpdate");
let ageFieldUpdate = document.getElementById("ageFieldUpdate");
let idFieldUpdate = document.getElementById("idFieldUpdate");
let idFieldDelete = document.getElementById("idFieldDelete");

let mainURL = 'https://localhost:44356/api/Proba/';


function RenderPeople(data) {
    console.dir(data);

    let htmlCode = "";
    htmlCode += "<table>";

    htmlCode += "<tr>";
    htmlCode += "<th>Id</th>";
    htmlCode += "<th>Name</th>";
    htmlCode += "<th>Age</th>";
    htmlCode += "</tr>";

    for (let i = 0; i < data.length; i++) {
        htmlCode += "<tr>";
        htmlCode += "<td>" + data[i].Id + "</td>";
        htmlCode += "<td>" + data[i].Name + "</td>";
        htmlCode += "<td>" + data[i].Age + "</td>";
        htmlCode += "</tr>";
    }
    htmlCode += "</table>";

    peopleDiv.innerHTML = htmlCode;

}

function GetPeoples() {
    $.ajax({
        url: mainURL + 'GetPeoples',
        type: 'GET',
        success: RenderPeople,
        error: function (jqXHR, textStatus, errorThrown) {
            alert("error");
        }
    });
}


function Success() {
    alert("Success");
    GetPeoples();
}

function InsertPeople() {
    let name = nameField.value;
    let age = ageField.value;

    $.ajax({
        url: mainURL + 'InsertPeople',
        type: 'POST',
        data: {
            Name: name,
            Age: age
        },
        dataType: 'json',
        success: Success,
        error: function (jqXHR, textStatus, errorThrown) {
            alert("error");
        }
    });
}

function UpdatePeople() {
    let name = nameFieldUpdate.value;
    let age = ageFieldUpdate.value;
    let id = idFieldUpdate.value;

    $.ajax({
        url: mainURL + 'UpdatePeople',
        type: 'POST',
        data: {
            Name: name,
            Age: age,
            Id: id
        },
        dataType: 'json',
        success: Success,
        error: function (jqXHR, textStatus, errorThrown) {
            alert("error");
        }
    });
}

function DeletePeople() {
    let id = idFieldDelete.value;

    $.ajax({
        url: mainURL + 'DeletePeople',
        type: 'POST',
        data: '=' + id,
        dataType: 'json',
        success: Success,
        error: function (jqXHR, textStatus, errorThrown) {
            alert("error");
        }
    });
}



function FindPeoplesById() {
    let id = idFieldFaind.value;

    $.ajax({
        url: mainURL + 'FindPeoplesById',
        type: 'POST',
        data: '=' + id,
        dataType: 'json',
        success: RenderPeople,
        error: function (jqXHR, textStatus, errorThrown) {
            alert("error");
        }
    });
}


